DROP VIEW vPersonne CASCADE;
DROP VIEW vClients CASCADE;
DROP VIEW vPersonnel CASCADE;
DROP TABLE Proprietaires CASCADE;
DROP TABLE Prescriptions CASCADE;
DROP TABLE Autorise CASCADE;
DROP TABLE Classes CASCADE;
DROP TABLE Especes CASCADE;
DROP TABLE Clients CASCADE;
DROP TABLE Veterinaires CASCADE;
DROP TABLE Assistants CASCADE;
DROP TABLE Animaux CASCADE;
DROP TABLE Medicaments CASCADE;
DROP TABLE Traitements CASCADE;



 


CREATE TABLE Classes(
nom VARCHAR(50) NOT NULL,
PRIMARY KEY (nom)
);


CREATE TABLE Especes(
nom VARCHAR(50) NOT NULL,
classe VARCHAR(50) NOT NULL,
PRIMARY KEY (nom),
FOREIGN KEY (classe) REFERENCES Classes(nom)
);


CREATE TABLE clients(
id INTEGER,
telephone INTEGER NOT NULL,
nom VARCHAR(50) NOT NULL,
prenom VARCHAR(50) NOT NULL,
date_naissance  DATE NOT NULL,
adresse VARCHAR(255),
PRIMARY KEY (id),
CHECK (telephone > 100000000 AND telephone < 999999999)
--CHECK (date_naissance < NOW() AND date_naissance > to_date('01-jan-1900')
);

CREATE TABLE Veterinaires(
id INTEGER,
telephone INTEGER  NOT NULL,
nom VARCHAR(50) NOT NULL,
prenom VARCHAR(50) NOT NULL,
date_naissance  DATE NOT NULL,
adresse VARCHAR(255),
specialite VARCHAR(50),
PRIMARY KEY (id),
FOREIGN KEY (specialite) REFERENCES Classes(nom),
CHECK (telephone > 100000000 AND telephone < 999999999)
--CHECK (date_naissance < NOW() AND date_naissance > to_date('01-jan-1900')
);

CREATE TABLE Assistants(
id INTEGER NOT NULL,
telephone INTEGER  NOT NULL,
nom VARCHAR(50) NOT NULL,
prenom VARCHAR(50) NOT NULL,
date_naissance  DATE NOT NULL,
adresse VARCHAR(255),
specialite VARCHAR(50),
PRIMARY KEY (id),
FOREIGN KEY (specialite) REFERENCES Classes(nom),
CHECK (telephone > 100000000 AND telephone < 999999999)
--CHECK (date_naissance < NOW() AND date_naissance > to_date('01-jan-1900')
);




CREATE TABLE Animaux(
id INTEGER NOT NULL,
nom VARCHAR(30) NOT NULL,
dernierPoids INTEGER  NOT NULL,
derniereTaille INTEGER  NOT NULL,
date_naissance INTEGER  NOT NULL,
espece VARCHAR(50) NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY (espece) REFERENCES Especes(nom),
CHECK (dernierPoids > 0),
CHECK (derniereTaille > 0)
--CHECK (date_naissance < NOW() AND date_naissance > to_date('01-jan-1900')
);


CREATE TABLE Proprietaires(
client INTEGER NOT NULL,
animal INTEGER NOT NULL,
PRIMARY KEY (client, animal),
FOREIGN KEY (client) REFERENCES Clients(id),
FOREIGN KEY (animal) REFERENCES Animaux(id)
);


CREATE TABLE Medicaments(
nomMolecule VARCHAR(50) NOT NULL,
effets VARCHAR(255) NOT NULL,
PRIMARY KEY (nomMolecule)
);


CREATE TABLE Autorise(
medicament VARCHAR(50) NOT NULL,
espece VARCHAR(50) NOT NULL,
PRIMARY KEY (medicament, espece),
FOREIGN KEY (medicament) REFERENCES Medicaments(nomMolecule),
FOREIGN KEY (espece) REFERENCES Especes(nom)
);

CREATE TABLE Traitements(
id INTEGER NOT NULL,
nom VARCHAR(100), 
debut DATE NOT NULL,
duree TIME NOT NULL,
veterinaire INTEGER NOT NULL,
animal INTEGER NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY (veterinaire) REFERENCES Veterinaires(id),
FOREIGN KEY (animal) REFERENCES Animaux(id)
--CHECK (duree > 0),
--CHECK (debut >= NOW())
);


CREATE TABLE Prescriptions(
traitement INTEGER NOT NULL,
medicament VARCHAR(50) NOT NULL,
quantite INTEGER  NOT NULL,
PRIMARY KEY (traitement, medicament),
FOREIGN KEY (medicament) REFERENCES Medicaments(nomMolecule),
FOREIGN KEY (traitement) REFERENCES Traitements(id),
CHECK (quantite > 0)
);

CREATE VIEW vClients AS
SELECT telephone, nom, prenom, date_naissance, adresse
FROM Clients
;

CREATE VIEW vPersonnel AS
SELECT telephone, nom, prenom, date_naissance, adresse, specialite
FROM Assistants 
UNION  
SELECT telephone, nom, prenom, date_naissance, adresse, specialite
FROM Veterinaires
;


CREATE VIEW vPersonne AS
SELECT telephone, nom, prenom, date_naissance, adresse
FROM vClients, vPersonnel
;



CREATE ASSERTION Especes_Classes CHECK
( SELECT espece FROM Classes = SELECT  nom AS espece FROM Especes);

CREATE ASSERTION Animal_Proprietaires CHECK
( SELECT id FROM Animaux = SELECT animal AS id FROM Proprietaires);

CREATE ASSERTION Client_Proprietaires CHECK
( SELECT id FROM Clients = SELECT  client AS id FROM Proprietaires);


CREATE ASSERTION Especes_Classes CHECK
( SELECT espece FROM Classes = SELECT  nom AS espece FROM Especes);


INSERT INTO Clients (id, telephone, nom, prenom, date_naissance, adresse)
VALUES
(1, '06102030400909', 'Lefevre', 'Pierre', '1990-05-02', '10 rue de Paris 75010 Paris'),
(2, '0610203050', 'Dupond', 'Anne', '1995-07-14', '50 rue de Nantes 75015 Paris'),
(3, '0614814050', 'Smith', 'Louis', '1980-10-28', '25 rue Saint Anne 75009 Paris');

INSERT INTO Classes(nom)
VALUES
('Mammiferes'),
('Poissons'),
('Oiseaux'),
('Reptiles');

INSERT INTO Especes(nom, classe)
VALUES
('Chien', 'Mammiferes'),
('Anaconda', 'Reptiles')
('Moineau', 'Oiseaux');

INSERT INTO Animaux(id, nom, dernierPoids, derniereTaille, date_naissance, espece)
VALUES
(1, 'Toutou', 15, 70, 2007, 'Chien'),
(2, 'Pioupiou', 0.3, 10, 2018, 'Moineau'),
(3, 'Hector', 3, 150, 2015, 'Anaconda');

INSERT INTO Proprietaires(client, animal)
VALUES
(1, 1),
(2, 2),
(1, 3),
(2, 1);

INSERT INTO Medicaments(nomMolecule, effets)
VALUES
('CH4O3', 'Créé des démangeaisons dans le cou'),
('AgC3+', 'Facilite le transit intestinal'),
('Au4Co2H2O', 'Tue les poux');

INSERT INTO Autorise(medicament, espece)
VALUES
('CH4O3', 'Chien'),
('AgC3', 'Anaconda'),
('Au4Co2H2O', 'Moineau'),
('Au4Co2H2O', 'Anaconda'),
('AgC3', 'Chien');

INSERT INTO Traitements(id, nom, debut, duree, veterinaire, animal)
VALUES
(1,'Traitement contre les poux', '2019-05-30', 30, 1, 1),
(2,'Traitement pour la diarrhée', '2019-03-29', 10, 1, 2),
(1,'Maux de tete', '2015-03-12', 5, 3, 3),
(1,'Mort aux rats', '2019-12-12', 115, 3, 1);

INSERT INTO Veterinaires (id, telephone, nom, prenom, date_naissance, adresse, specialite)
VALUES
(1, '0616814835', 'Martin', 'John', '1970-01-20', '10 rue Saint Anne 75009 Paris', 'Oiseaux'),
(2, '061648425', 'Martin', 'Alex', '1987-05-21', '15 faubourg Saint Martin 60200 Compiegne', 'Mammiferes'),
(3, '0770028656', 'Bruley', 'Philippe', '1973-05-31', '20 rue de la gare 60200 Compiegne', 'Reptiles');

INSERT INTO Assistants (id, telephone, nom, prenom, date_naissance, adresse, specialite)
VALUES
(3, '061644542', 'Dubois', 'Thomas', '1980-08-10', '15 faubourg Saint Martin 75010 Paris', 'Oiseaux'),
(4, '0854345689', 'Picard', 'Jacques', '1800-30-12', '19 rue Saint Gervais 60700 Pontpoint', 'Mammiferes'),
(5, '+336608783', 'Bouvier', 'Mathieu', '2000-01-10', '2 rue du Dahomey 60200 Compiegne', 'Poissons');
