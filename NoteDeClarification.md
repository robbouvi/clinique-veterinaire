Note de clarification

La liste des objets:
- Clients
- Personnels (Vétérinaires ou Assistants) 
- Animaux Traités
- Médicaments administrés
- Traiments préscrits

Propriétés des objets: 
Client
- Nom (Chaîne de caractère)
- Prenom (Chaîne de caractère)
- Date de naissance (Date)
- Adresse 
- Numéro de téléphone

Personnel
- Nom (Chaîne de caractère)
- Prenom (Chaîne de caractère)
- Date de naissance (Date)
- Adresse 
- Numéro de téléphone
- Poste (Vétérinaires ou Assistants) 
- Spécialité sur les espèces mieux traités (Mammifères, Reptiles, Oiseaux, Autres)

Animal traité
- Nom
- Espèce (Mammifères, Reptiles, Oiseaux, Autres)
- Dernier poids mesuré
- Dernier taille mesuré
- Date de naissance (Année ou inconnu)
- Propritaire (Non personnel)

Médicament administré
- Nom de molécule
- Description des effets
- Espèces autorisés (Mammifères, Reptiles, Oiseaux, Autres)

Traiment préscrit
- Veritérinaire (Unique)
- Animal (Unique)
- Début
- Durée
- Médicaments (Quantités journalières)

Listes des fonctionalités:
- Un gestionnaire pourra ajouter et mettre à jour le personnel, les clients, les animaux traités et les médicaments.
- Le site permettra de pouvoir obtenir facilement des informations statistiques, comme la quantité de chaque type de médicament prescrit pour un animal donné, ou la quantité d'un médicament prescrit au total dans la clinique, ou les poids et taille moyenne des animaux d'une espèce traités.
- Le site permettra au personnel vétérinaire de prescrire des traitements
